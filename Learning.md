# React

##  Advantages of using JSX

- JSX means you can write JavaScript code inside the HTML.
-  If you want to create a large Application in react so without JSX handle all component pieces of code are more difficult so using the JSX is handled to easily large applications.
- JSX is together with your HTML code and JavaScript code.
- React component has render function which is used to translate your JSX code into regular Javascript code runtime because the browser does not understand your JSX code. 
- We can not execute JSX code directly inside the browser we required the compiler for executing JSX code to JavaScript babel compiler convert your JSX code into JavaScript here Babel working like Transfer JSX code into JavaScript.
- JSX helps for code simpler and Attractive when writing large pieces of code for big React Application 
- JSX is Faster compare normal JavaScript code
- using JSX you can create faster Application in React 


## React element

- Elements are the smallest building blocks of React apps.
- An element specifies what should be there in our UI.
- An Element is a plain object describing what we want to appear in terms of the DOM nodes.
- An Element can be as simple as the one or an element can contain elements inside it.


## All the situation in which React re-renders the component

- Every re-render in React starts with a state change.
- It's the only “trigger” in React for a component to re-render.
- In React, every state variable is attached to a particular component instance.
- The point of a re-render is to figure out what needs to change.
- Update in State
- Update in prop
- Re-rendering of the parent component


##  when we need a functional and when we need a class component

- Class Component – The components created using the ES2015 class template are known as class components.
- Function Component – The components created using the JavaScript function syntax are functional components.
- Class Components should be preferred whenever we have the requirement with the state of the component.
- Functional components are used when the components have from zero to minimal state management.
- use functional components wherever possible, because they support hooks, and hooks are better than alternatives


## Difference between a functional and class component?

- A functional component is just a plain JavaScript pure function that accepts props as an argument and returns a React element(JSX).
- A class component requires you to extend from React. Component and create a render function which returns a React element.
- There is no render method used in functional components.
- It must have the render() method returning JSX (which is syntactically similar to HTML)
- Functional component run from top to bottom and once the function is returned it cant be kept alive.
- Class component is instantiated and different life cycle method is kept alive and being run and invoked depending on phase of class component.


## Why the component name should starts with uppercase?

- If you start a component name with a lowercase letter, it will be treated like a built-in element like a <div> or a <span> .
- This is because of the way JSX works.


## Why we need ReactDOM?

- ReactDOM is used to render components and elements on the web. 
- It is a part of the React library used to create user interfaces and dynamic web applications.
- We need ReactDOM to interact and manipulate the DOM structure, including the elements and components that are rendered on the page.
- It’s also used to control the state of the components and elements on the page. 


##  Difference between state and props?

- The state is an updated structure that is used to contain and can modify data or information about the component over time.
- “Props” is a special React keyword for proprietary purposes, used for data transmission from component to component. 
- State is managed within the component
- Props gets passed to the component
- State can be changed(mutable)
- Props are read only and cannot be changed (immutable)
- State is controlled by react components
- Props are controlled by whoever renders the components
- State can used to display changes with the component
- Props are used to communicate between components


## Difference between JSX and HTMl

- Hypertext Markup Language (HTML) is the standard language for documents that determine the structure of a web page.
- JSX  is an extension of JavaScript that allows developers to write HTML right within JavaScript.
-  So when you're writing JSX, technically you're writing JavaScript and HTML together.
- In terms of support by browsers, HTML is supported by all of them. 
- JSX, on the other hand, was never really intended to be, so you need a compiler like Babel or Webpack to transpile JSX into the JavaScript that browsers understand.


## Why we use PropTypes?

- Since JavaScript doesn’t have a built-in type checking solution, many developers use extensions like TypeScript and Flow. However, React has an internal mechanism for props validation called PropTypes.
- PropTypes is React’s internal mechanism for adding type checking to component props. 
- When props are passed to a React component, they are checked against the type definitions configured in the propTypes property. - When an invalid value is passed for a prop, a warning is displayed on the JavaScript console.


## setState is asynchronous in nature 

- When you call setState to update the state of a component, the state may not be immediately updated, and the component may not re-render immediately.
-  If setState was synchronous, it could result in unnecessary re-renders of a component. For example, if multiple state updates were called sequentially, each update could trigger a separate re-render, even though the final state would have been the same
- Example

assuming this.state.count === 0

                                                this.setState({count: this.state.count + 1});
                                                this.setState({count: this.state.count + 1});
                                                this.setState({count: this.state.count + 1});

                                                // this.state.count === 1, not 3

                                                Solution
                                                this.setState((prevState, props) => ({
                                                count: prevState.count + props.increment
                                                }));


## Why we should not update the state directly?

- The state of a component is managed internally by React. 
- Updating the state of a component directly can have unintended consequences that can be difficult to debug.
- React, keeps a track record of all its virtual DOM. 
- Whenever a change happens, all the components are rendered and this new virtual DOM is then compared with the old virtual DOM. Only the differences found are then reflected in the original DOM.
- So, it’s obvious from the statement that if we mutate the state directly, it will change the reference of the state in the previous virtual DOM as well. 
- So, React won’t be able to see that there is a change of the state and so it won’t be reflected in the original DOM until we reload.


## Form validation in React

- Form validation in React allows an error message to be displayed if the user has not correctly filled out the form with the expected type of input.
- React Hook Form is a library that helps you validate forms in React.
- Set up a form component: Create a form component using React, which includes input fields, submit button, and event handlers.
-  Implement form validation logic inside the handleSubmit function or as part of the handleChange function. 
- You can use JavaScript regular expressions or other validation libraries to validate the form data based on your requirements.


## What is Virtual DOM and how it works?

- The virtual DOM is a much lighter replica of the actual DOM in the form of objects. 
- The virtual DOM can be saved in the browser memory and doesn’t directly change what is shown on the user’s browser.
- The virtual DOM provides a mechanism that allows the actual DOM to compute minimal DOM operations when re-rendering the UI.
- When an element in the real DOM is changed, the DOM will re-render the element and all of its children. 
- When it comes to building complex web applications with a lot of interactivity and state changes, this approach is slow and inefficient.
- After the virtual DOM is updated, React compares it to a snapshot of the virtual DOM taken just before the update, determines what element was changed, and then updates only that element on the real DOM. 
- This is one method the virtual DOM employs to optimize performance.


## Difference between controlled and uncontrolled component?

 #### Controlled component

- Managed by React state.
- Data flows from parent component to component
- Easier to debug
- Generally faster as there's less state management
- 	Less complex code
- 	Parent component updates state on user interaction

#### Uncontrolled component

- Managed by component's own internal state
- Component updates own internal state on user interaction
- Data flows within the component
- More difficult to debug
- Generally slower as there's more state management
- 	More complex code


## Why we need to pass key prop when displaying a list?

- A key is a unique attribute that needs to be included in lists. 
- Keys help React identify which items have changed (added/removed/re-ordered).
-  To give a unique identity to every element inside the array, a key is required.
- It is important in the aspect of re-rendering collections. In simple words, React uses keys to pinpoint which of the element in the list collection is changed and needs to be re-rendered instead of re-rendering the whole collection. 
- Therefore boosting the performance of the react application.



## Explain different lifecycle methods and their use-cases?

- Lifecycle methods are series of events that happen throughout the birth, growth, and death of a React component.
- In React, components go through a lifecycle of events:

    Mounting (adding nodes to the DOM)
    Updating (altering existing nodes in the DOM)
    Unmounting (removing nodes from the DOM)
    Error handling (verifying that your code works and is bug-free)

- Mounting a component is like bringing a newborn baby into the world. This is the component’s first glimpse of life. At this phase, the component, which consists of your code and React’s internals, is then inserted into the DOM.
- After the mounting phase, the React component “grows” during the updating phase. Without updates, the component would remain as it was when it was initially created in the DOM. 
- The final phase is called the unmounting phase. At this stage, the component “dies”. In React lingo, it is removed from  the DOM.
- Error handling phase. This occurs when your code doesn’t run or there’s a bug somewhere. 


## Reasons to use context in React and how it works?

- Context provides a way to pass data through the component tree without having to pass props down manually at every level.
- When you need to tell a child component about a change in the state notified by the principal and root components, we pass state from one component to another until we reach the child component. 
- Context's job in React is to share data or the app state across all the app components. 
- This means that the props can be accessed from any component. Therefore, no need to pâss props through other components to send it at the end to our destination component.
- Less code, less debugging hassle, and more code visibility.
- You want to share data with many components at different nesting levels. (Like a theme, for example, when you change from white to black, all components need to switch their colors, icon, and so on)
- When the same prop (data) is passed through several components as an intermediate:


## Use case of React.Fragment?

- The most common use case for React fragments is probably to return multiple elements. 
- React fragments can be used when elements are rendered conditionally. 
- They can make rendering groups of elements a lot easier as they remove the use of extra div elements.


## Possible to loop inside the JSX give an example of how to do this?

- Two Ways to Loop Inside React JSX

1. Using For Loop
2. Using Map Function

                    import React from ‘react’

                    const RenderList = props => {
                    const Languages= ["French", "German", "English", "Spanish" , "Mandarin"];

                    return (
                        <ul>
                        {Languages.map(Languages => (
                            <li>{Languages}</li>
                        ))}
                        </ul>
                    );
                    };


## Advantages of using Hooks?

- You can write concise and clearer code.
- Hooks are simpler to work with and test. Code would appear cleaner and easier to read.
- It simplifies how to make code more composable and reusable. 
- It revolutionizes the way you write components


## Explain how to use useEffect?

- The useEffect Hook allows you to perform side effects in your components.
- Examples of side effects are: fetching data, directly updating the DOM, and timers.
- useEffect runs on every render. That means that when the count changes, a render happens, which then triggers another effect.
- We should always include the second parameter which accepts an array. We can optionally pass dependencies to useEffect in this array.
- Passing an empty array results in one render only


## All the hooks in React

- useState: This hook allows you to manage local state within a functional component. It returns an array with two values: the current state value and a function to update the state.
- useEffect: This hook allows you to perform side effects, such as fetching data, subscribing to events, or updating the DOM, after the component has rendered. It takes a callback function and an array of dependencies as arguments.
- useContext: This hook allows you to access a context value from the nearest context provider in the component tree. It takes the context object as an argument and returns the current context value.
- useReducer: This hook allows you to manage complex state with a reducer function, similar to how you would manage state with a Redux store. It returns the current state value and a dispatch function to update the state.
- useRef: This hook allows you to create a mutable ref that can be used to store a value across renders, similar to how you would use instance variables in class components.


## Difference between DOM element and react element

 - DOM element represents an actual element in the web browser's DOM tree. 
 - React element represents a virtual representation of a UI component in a React application. React elements are used to describe the structure and properties of UI components in a declarative manner, and they are rendered into DOM elements using React's rendering methods like ReactDOM.render().
 - React elements are immutable and cannot be directly manipulated like DOM elements.


 ## Runtime error happens in the JSX? How can we fix that issue?

 - To solve the error "Module not found: Error: Can't resolve 'react/jsx-runtime'", make sure to update the react package by opening your terminal in your project's root directory and running the command npm install react@latest react-dom@latest and restart your dev server.
 - Delete your node_modules and reinstall your dependencies
 - Make sure react is in your dependencies object
 

 ## Rules of hooks?

 - There are 3 rules for hooks:

    1. Hooks can only be called inside React function components.
    2. Hooks can only be called at the top level of a component.
    3. Hooks cannot be conditional


## Why we can put hooks in a if statement?

- If Hooks are called conditionally within an if statement, their behavior may not be consistent, as they may not be called in every render cycle, resulting in unexpected state updates or side-effects.
- Hooks are designed to maintain their state between renders of a component. If a Hook is called within an if statement, it may be skipped in certain render cycles, leading to inconsistent state management.


## Advantage of using React.StrictMode?

- StrictMode is a React Developer Tool, primarily used to identify potential issues in a web application.
- React StrictMode shows the application's potential flaws. 
- As a result, you'll be able to develop more understandable and secure programmes.
- You don't need to import this functionality separately because it's part of the React package.
- StrictMode does not render any visible elements in the DOM in development mode, but it enables checks and gives warnings. 


## Can you create the production build of a React app?

- Yes, we can create a production build of a React app using the build tools provided by React, such as webpack or Create React App.
- Install dependencies, config build settings, run the build command
- Check the build output: After the build process completes, the production build of your React app will be generated in the designated output folder, usually called build or dist.
- Deploy the production build: Once you have the production build of your React app, you can deploy it to a web server or a hosting service of your choice. 


## Difference between Development and Production?

- Development is the stage where software is being created, tested, and refined by developers. It is typically done in a local development environment or a development server.
- Development environments are optimized for ease of development, with features such as hot reloading, debugging tools, and development-oriented configurations to facilitate faster development and debugging.
- Production is the stage where the software is deployed to a live environment and made available for end-users to access and interact with.
- Production environments are optimized for performance, security, and stability, with production-grade configurations and settings to ensure the smooth operation of the software in a live environment.